package com.socialuni.social.sdk.dao.DO.base;


import java.io.Serializable;

public interface BaseModelParentDO {

    void setTalkId(Integer talkId);

    void setMessageId(Integer messageId);

    void setCommentId(Integer commentId);

    void setUserImgId(Integer userImgId);
}
