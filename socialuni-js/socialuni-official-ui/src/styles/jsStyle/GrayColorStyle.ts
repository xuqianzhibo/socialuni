export default class GrayColorStyle {
    static gray400 = {
        color: '#9ca3af'
    }

    static blueGray700 = {
        color: '#334155'
    }
}
